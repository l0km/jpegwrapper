echo off 
echo make jpegwrapper VS2015 project
if not defined VS140COMNTOOLS (
	echo vs2015 NOT FOUND.
	exit /B -1
)
echo vs2015 found.
where cmake
if errorlevel 1 (
	echo cmake NOT FOUND.
	exit /B -1
)
echo cmake found
set sh_folder=%~dp0
pushd %sh_folder%
if exist project.vs2015 rmdir project.vs2015 /s/q
mkdir project.vs2015
pushd project.vs2015
if not defined VisualStudioVersion (
	echo make MSVC environment ...
	call "%VS140COMNTOOLS%..\..\vc/vcvarsall" x86_amd64
)
rem 定义编译的版本类型(DEBUG|RELEASE)
set build_type=Debug
rem 如果输入参数1为"RELEASE"(不区分大小写)则编译RELEASE版本
if /I "%1" == "RELEASE" ( set build_type=Release)
if /I "%1" == "DEBUG" ( set build_type=Debug)
echo build_type=%build_type%
rem 是否编译测试程序
set build_test=True

echo creating x86_64 Project for Visual Studio 2015 ...

conan install %sh_folder% --build missing -s compiler=msvc -s compiler.version=14 -of . -o build_test=%build_test% -s build_type=%build_type% || exit /B

cmake .. -G "Visual Studio 14 2015 Win64" ^
	-DAUTO_BUILD_TEST=ON ^
	-DCMAKE_TOOLCHAIN_FILE=./conan_toolchain.cmake ^
	-DAUTO_BUILD_TEST=%build_test% ^
	-DCMAKE_INSTALL_PREFIX=..\release\jpegwrapper-windows-vc-x86_64 || exit /B

cmake --build   . --config %build_type% || exit /B
cmake --install . --config %build_type% || exit /B
popd
popd