echo off
setlocal 
echo make test MinGW project
where gcc
if errorlevel 1 (
	echo MinGW/gcc NOT FOUND.
	exit /B -1
)
echo MinGW/gcc found.
where cmake
if errorlevel 1 (
	echo cmake NOT FOUND.
	exit /B -1
)
echo cmake found
set sh_folder=%~dp0
rem 删除最后的 '\'
set sh_folder=%sh_folder:~0,-1%
rem 获取所在文件夹名
for %%a in ("%sh_folder%") do set folder_name=%%~nxa
rem 定义编译的版本类型(DEBUG|RELEASE)
set build_type=Debug
rem 如果输入参数1为"RELEASE"(不区分大小写)则编译RELEASE版本
if /I "%1" == "RELEASE" ( set build_type=Release)
if /I "%1" == "DEBUG" ( set build_type=Debug)
echo build_type=%build_type%
rem 是否编译测试程序
set build_test=True

pushd "%sh_folder%\.."
if exist "%folder_name%.mingw" rmdir "%folder_name%.mingw" /s/q
mkdir "%folder_name%.mingw"
pushd "%folder_name%.mingw"
set sh_folder=%sh_folder:\=/%
echo creating x86_64 Project for MinGW ...

for /f "delims=" %%i in ('gcc -dumpmachine') do set machine=%%i
for /f "delims=" %%i in ('gcc -dumpversion') do set version=%%i

rem 删除最后两个字符
set version=%version:~0,-2%

conan install %sh_folder% --build missing -s compiler=gcc -s compiler.version=%version%  -of . -o build_test=%build_test% -s build_type=%build_type% || exit /B


cmake %sh_folder% -G "Eclipse CDT4 - MinGW Makefiles" ^
	-DCMAKE_BUILD_TYPE=%build_type% ^
	-DCMAKE_TOOLCHAIN_FILE=./conan_toolchain.cmake ^
	-DAUTO_BUILD_TEST=%build_test% ^
	-DCMAKE_INSTALL_PREFIX=%sh_folder%/release/jpegwrapper-%machine% || exit /B
	
cmake --build   . --config %build_type% || exit /B
cmake --install . --config %build_type% || exit /B
popd
popd

endlocal
goto :eof
