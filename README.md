# jpegwrapper

基于libjpeg-turbo,openjpeg 实现对jpeg,jpeg2000(j2k)图像的内存编解码(C++11封装)。

**NOTE:**

如果你只是想实现JPEG图像的内存解码，其实不需要这么复杂的，[CImg](http://cimg.eu/) 就可以解决你的问题,参见我的博客:

[《CImg:插件(plugin)使用说明塈实现JPEG图像内存编码/解码》](https://blog.csdn.net/10km/article/details/82925459)

Conan是一个面向 C 和 C++ 开发人员的软件包管理器，Conan 可以为不同的构建配置管理任意数量的不同二进制文件，包括不同的架构、编译器、编译器版本、运行时、C++ 标准库等。它可以大大降低项目在跨平台构建的复杂度，提高工作效率

从 `jpegwrapper-1.1.0`版本开始，jpegwrapper支持conan构建，所以构建需要conan 2.x支持。如果安装conan 参见 [《conan入门(一):conan 及 JFrog Artifactory 安装_conan教程-CSDN博客》](https://blog.csdn.net/10km/article/details/122987204)

如果需要非conan支持的代码，请切换到`jpegwrapper-1.0.0`版本。

## 代码下载

    git clone https://gitee.com/l0km/jpegwrapper.git

## 命令行编译

要求cmake (3.15以上版本)
### windows

Visual Studio 2015编译,windows CMD下执行[`make_msvc_project.bat`](make_msvc_project.bat)


使用MinGW编译，执行[mingw_build.bat](mingw_build.bat),生成32还是64位程序取决于MinGW编译器版本，对于`SJLJ`版本编译器可以同时生成32/64位库。

### linux/gcc

linux下编译参见脚本 [gnu_build.sh](gnu_build.sh),编译时可能需要根据自己linux系统的实际情况修改脚本。

生成的静态库在release文件夹下

linux下编译测试程序 需要libX11支持，如果没有，请安装,例如(CentOS)：

	sudo yum install -y libX11-devel

## 代码结构

核心代码很少：

[j2k_mem.h](jpegwrapper/jpeg_mem.h):jpeg图像内存读写实现

[j2k_mem.h](jpegwrapper/j2k_mem.h):jpeg2000图像内存读写实现

[CImgWrapper.h](jpegwrapper/CImgWrapper.h):基于CImg进一步对jpeg,j2k图像读写做便利化封装，因为CImg.h体积很大，所以使用该头文件，会导致编译时间较长

## 调用方式

参见附带的测试代码 [testCImg.cpp](jpegwrapper/testCImg.cpp)

## cmake引用jpegwrapper 库示例

cmake查找 jpegwrapper 库的示例：

	# CONFIG模式查找 jpegwrapper 依赖库
	# 需要在 CMAKE_MODULE_PATH 指定 FindTurboJPEG.cmake的位置，本例中的位置在项目根目录下/cmake/Modules
	# 需要在 CMAKE_PREFIX_PATH 指定 jpegwrapper以及其依赖库turbojpeg,openjpeg的安装位置
	find_package(jpegwrapper CONFIG REQUIRED)

cmake脚本中引用 jpegwrapper 库的示例：

	# 引用jpegwrapper静态库
	target_link_libraries(test_jpegwrapper jpegwrapper::jpegwrapper)
	# 增加 openjpeg include
	target_include_directories (test_jpegwrapper PUBLIC ${OPENJPEG_INCLUDE_DIRS})


cmake脚本中引用 jpegwrapper 库的完整示例参见 [test/CMakeLists.txt](test/CMakeLists.txt)

创建调用示例的VS2015工程的过程参见 [test/make_msvc_project.bat](test/make_msvc_project.bat)

生成unix Makefile过程参见：
[test/make_unix_makefile.sh](test/make_unix_makefile.sh)



