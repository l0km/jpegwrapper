#cmake file for project opencl
#author:guyadong
#created:2016/01/09
##! 用于velrelcm.sh 的 变量搜索正则表达式, 不要轻易修改
##! @PRJNAME_REG@          (project\s*\(\s*)([[:alnum:]-]+)(\s+.*\))
##! @VERSION_REG@          (project\s*\(\s*[[:alnum:]-]+\s+VERSION\s+)(\S*)(.*\))
##! @SNAPSHOT_REG@         (project\s*\(\s*[[:alnum:]-]+\s+VERSION\s+[[:digit:]]+\.[[:digit:]]+\.[[:digit:]]+)(\.1)?(.*\))
cmake_minimum_required( VERSION 3.0...3.15 )
cmake_policy(SET CMP0048 NEW)
# 3.0以上版本才允许使用VERSION option
project(jpegwrapper VERSION 1.1.1.1 LANGUAGES CXX)
#判断编译类型和版本是否满足编译要求
list(FIND CMAKE_CXX_COMPILE_FEATURES cxx_std_11 _cxx11_enable)
if(NOT _cxx11_enable)
	message(FATAL_ERROR "Compiler not supported C++ 11 standard")
endif()
unset(_cxx11_enable)
# 要求编译器支持C++11
set(CMAKE_CXX_STANDARD 11)
SET(CMAKE_CXX_STANDARD_REQUIRED ON)
if(MSVC)
	#关闭C4819警告
	add_definitions("/wd4819")
	message(STATUS "optional:/wd4819")
	#关闭CRT_SECURE警告
	add_definitions(-D_CRT_SECURE_NO_WARNINGS)
	message(STATUS "optional:-D_CRT_SECURE_NO_WARNINGS")
endif(MSVC)

##############设置目标文件生成位置#####################
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY "${PROJECT_BINARY_DIR}/bin")
set(CMAKE_LIBRARY_OUTPUT_DIRECTORY "${PROJECT_BINARY_DIR}/bin")
set(CMAKE_ARCHIVE_OUTPUT_DIRECTORY "${PROJECT_BINARY_DIR}/lib")
set( SUBDIRECTORIES jpegwrapper)
foreach( subdir ${SUBDIRECTORIES} )
	if( IS_DIRECTORY "${CMAKE_CURRENT_LIST_DIR}/${subdir}" )
		MESSAGE( STATUS "Found sub project ${subdir}, adding it" )
		add_subdirectory( ${subdir} )
	endif()
endforeach()

