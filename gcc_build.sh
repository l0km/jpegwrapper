#!/bin/bash
# 根据$1参数 编译DEBUG|RELEASE版本,默认RELEASE
echo build jpegwrapper 

sh_folder=$(dirname $(readlink -f $0))
pushd $sh_folder
# 定义编译的版本类型(DEBUG|RELEASE)
build_type=Release
typeset -u arg1=$1
[ "$arg1" = "DEBUG" ] && build_type="Debug"
echo build_type=$build_type
# 是否编译测试程序
build_test=True

[ -d build ] && rm -fr build
mkdir build
pushd build

conan install .. --build missing -of . -o build_test=$build_test -s build_type=$build_type || exit

cmake .. -G "Unix Makefiles" \
	-DCMAKE_BUILD_TYPE=$build_type \
	-DCMAKE_TOOLCHAIN_FILE=./conan_toolchain.cmake \
	-DCMAKE_INSTALL_PREFIX=../release/jpegwrapper-$(gcc -dumpmachine) || exit

cmake --build   . --config $build_type || exit
cmake --install . --config $build_type || exit

popd
#rm -fr build
popd
