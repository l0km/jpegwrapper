from conan import ConanFile
from conan.tools.cmake import CMake, CMakeDeps, CMakeToolchain
from conan.tools.env import VirtualBuildEnv

class JpegwrapperConan(ConanFile):
    name = "jpegwrapper"
    version = "1.1.1-dev"
    # Optional metadata
    license = "BSD-2-Clause"
    author = "guyadong 10km0811@sohu.com"
    url = "https://gitee.com/l0km/jpegwrapper"
    description = "Based on libjpeg turbo, openjpeg implements memory encoding and decoding (C++11) of jpeg, jpeg2000 (j2k) images."
    topics = ("jpeg","j2k","codec")

    tool_requires = "cmake/[>=3.15.7]"
    package_type = "static-library"
    # Binary configuration
    settings = "os", "compiler", "build_type", "arch"
    options = {"fPIC": [True, False],
        # 是否编译测试代码
        "build_test": [True, False]}
    default_options = {"fPIC": True, "build_test": False}
    # Sources are located in the same place as this recipe, copy them to the recipe
    exports_sources = "CMakeLists.txt", "jpegwrapper/*","jpegwrapper/CMakeLists.txt","cmake/config.cmake.in"

    def config_options(self):
        if self.settings.os == "Windows":
            del self.options.fPIC

    def requirements(self):
        self.requires("common_source_cpp/1.0.2", transitive_headers=True)
        self.requires("libjpeg-turbo/2.1.5", transitive_headers=True)
        self.requires("openjpeg/2.3.1", transitive_headers=True)
        if self.options.build_test :
            self.requires("cimg/2.8.3")

    def generate(self):
        tc = CMakeToolchain(self)
        tc.variables["AUTO_BUILD_TEST"] = self.options.build_test
        tc.generate()

        cd = CMakeDeps(self)
        cd.generate()

        env = VirtualBuildEnv(self)
        env.generate()

    def build(self):
        cmake = CMake(self)
        cmake.configure()
        cmake.build()

    def package(self):
        cmake = CMake(self)
        cmake.install()

    def package_id(self):
        del self.info.options.build_test

    def package_info(self):
        self.cpp_info.libs = ["jpegwrapper"]
        # 指定将安装路径加入CMAKE_PREFIX_PATH,以便执行find_package时能扫描到安装路径
        self.cpp_info.builddirs.append(self.package_folder)
        # 抑制conan生成jpegwrapper-config.cmake使用jpegwrapper自己生成的cmake配置文件 
        self.cpp_info.set_property("cmake_find_mode", "none")
        # cmake_find_mode为none时不需要配置此参数
        #self.cpp_info.requires = ["common_source_cpp::common_source_cpp","libjpeg-turbo::libjpeg-turbo","openjpeg::openjpeg"]
