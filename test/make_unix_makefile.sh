#!/bin/bash
sh_folder=$(dirname $(readlink -f $0))
folder_name=$(basename $sh_folder) 
# 定义编译的版本类型(DEBUG|RELEASE)
build_type=Debug
typeset -u arg1=$1
[ "$arg1" = "DEBUG" ] && build_type=Debug
[ "$arg1" = "RELEASE" ] && build_type=Release
echo build_type=$build_type
# 是否编译测试程序
build_test=True

pushd $sh_folder/..

[ -d $folder_name.gcc ] && rm -fr $folder_name.gcc
mkdir $folder_name.gcc

pushd $folder_name.gcc
conan install $sh_folder --build missing -of . -o build_test=$build_test -s build_type=$build_type || exit
cmake "$sh_folder" -G "Eclipse CDT4 - Unix Makefiles" \
	-DCMAKE_BUILD_TYPE=$build_type \
	-DCMAKE_TOOLCHAIN_FILE=./conan_toolchain.cmake || exit
	
cmake --build . --config %build_type% || exit

popd
popd

